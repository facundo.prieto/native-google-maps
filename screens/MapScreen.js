import React, {Component, useState, useEffect} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import MapView, { PROVIDER_GOOGLE , Marker} from 'react-native-maps';

export class MapScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            region: {},
        }
    }

    LoadingScreen = ()=>{
        this.props.navigation.navigate('LoadingScreen');
    }
      
    onRegionChange(region) {
        console.log(region);
        this.setState({ region: region });
    }

  render() {
    //console.log(this.props.navigation.getParam('location'));

    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                initialRegion={this.props.navigation.getParam('location')}
                onRegionChange={this.onRegionChange.bind(this)}
                >
                <Marker coordinate={this.props.navigation.getParam('location')} />
            </MapView>
            <Button title="Volver a LoadingScreen" onPress={() => this.LoadingScreen()} />
        </View>
    );
  }
}

export default MapScreen;

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
});
