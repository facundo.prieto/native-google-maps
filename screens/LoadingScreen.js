import React, {Component} from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Button} from 'react-native';

export class LoadingScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            locations : [
                {"latitude": -34.90542663731537, "latitudeDelta": 0.001, "longitude": -56.18677906958377, "longitudeDelta": 0.001},
                {"latitude": -34.90440541448011, "latitudeDelta": 0.001, "longitude": -56.18153275921941, "longitudeDelta": 0.001},
                {"latitude": -34.90544696617896, "latitudeDelta": 0.001, "longitude": -56.18638286367059, "longitudeDelta": 0.001},
                {"latitude": -34.9057730700026 , "latitudeDelta": 0.001, "longitude": -56.19069283828139, "longitudeDelta": 0.001}
            ]
        }
    }

    MapScreen(i){
        this.props.navigation.navigate('MapScreen', {location: this.state.locations[i]});
    }

    render() {
        return ( 
            <View style={styles.container}>
                <Text>Elegir restaurante:</Text>
                <Text> </Text>
                <Button title="McDonalds Ejido" onPress={() => this.MapScreen(0)} />
                <Text> </Text>
                <Button title="DONUT CITY" onPress={() => this.MapScreen(1)} />
                <Text> </Text>
                <Button title="Mostaza" onPress={() => this.MapScreen(2)} />
                <Text> </Text>
                <Button title="La Cigale" onPress={() => this.MapScreen(3)} />
            </View>
        );
    }
}

export default LoadingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
  