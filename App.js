import React, {Component} from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import LoadingScreen from './screens/LoadingScreen';
import MapScreen from './screens/MapScreen';

const AppSwitchNavigator = createSwitchNavigator({
  LoadingScreen: LoadingScreen,
  MapScreen: MapScreen
});

const AppNavigator = createAppContainer(AppSwitchNavigator);

export class App extends Component {
  render() {
    return ( 
      <AppNavigator/> 
      );
  }
}

export default App;