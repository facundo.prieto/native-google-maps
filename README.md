# Prueba de concepto (React Native CLI)
Mapas de Google desde react-native con React Native CLI  

[Documentación](https://github.com/react-native-maps/react-native-maps)  

## Crear un proyecto de React Native e instalar react-native-maps  

Desde una terminal ubicada en la carpeta donde queremos crear el proyecto ejecutar:  
```
npx react-native init nativegooglemaps --verbose  
cd nativegooglemaps  
npm install react-native-maps --save-exact  
```

## Agregar navecación para pruebas:  

Desde una terminal ubicada en la carpeta del proyecto ejecutar:  
```
npm install react-navigation  
npm install react-native-elements  
npm install react-navigation-stack  
```

Crear la carpeta "./screens", y dentro poner una pantalla donde irá el mapa, y una pantalla de carga con varias ubicaciones seteadas en el estado y botones que las envían por parámentro a la pantalla de mapa  
	
Cambiar el App.js por un navegador entre esas pantallas usando createAppContainer y createSwitchNavigator de react-navigation  

## Configuraciones necesarias: 

[Documentación](https://github.com/react-native-maps/react-native-maps/blob/master/docs/installation.md)

Agregar en el archivo `/android/build.gradle`:  

```
ext {
...
	supportLibVersion   = "28.0.0"
	playServicesVersion = "17.0.0"
	androidMapsUtilsVersion = "+0.5"
}
```

Ir a la consola de la nube de google (https://console.cloud.google.com/) y crear o seleccionar un proyecto.  
Desde el menú de API y servicios:  
- en la opción Biblioteca, habilitar la API "Maps SDK for Android"  
- en la opción Credenciales, crear una clave de API.  

Agregar la API key en los siguientes tags en el archivo `android/app/src/main/AndroidManifest.xml`:  

```
<application>
...
   <meta-data
     android:name="com.google.android.geo.API_KEY"

     	android:value="Tu Google maps API Key Aquí"/>

   <uses-library android:name="org.apache.http.legacy" android:required="false"/>
...
</application>
```


## Agregar el mapa: 

En la pantalla de mapa, agregar el siguiete código obtenido de la documentación:  

```
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
...
const styles = StyleSheet.create({
	container: {
		...StyleSheet.absoluteFillObject,
		height: 400,
		width: 400,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	map: {
		...StyleSheet.absoluteFillObject,
	},
});
...
export default () => (
	<View style={styles.container}>
		<MapView
		provider={PROVIDER_GOOGLE} // remove if not using Google Maps
		style={styles.map}
		initialRegion={{
			latitude: 37.78825,
			longitude: -122.4324,
			latitudeDelta: 0.015,
			longitudeDelta: 0.0121,
		}}
		>
		</MapView>
	</View>
);
```

Para almacenar la localización actual del mapa agrego una fucnión que guarde en el estado la region, y en la propiedad onRegionChange del MapView la llamo:  
```
onRegionChange(region) {
	//console.log(region);
	this.setState({ region: region });
}
...
<MapView onRegionChange={this.onRegionChange.bind(this)} />
```

Para renderizar las ubicaciones enviadas por parámetro, se cambia la propiedad "initialRegion" por `this.props.navigation.getParam('location')`  

Y para agregar un marcador en esa ubicaciónm agregar dentro del MapView la siguiente etiqueta:
```
<Marker coordinate={this.props.navigation.getParam('location')} />
```


En la [documentación](https://github.com/react-native-maps/react-native-maps) hay ejemplos de como agregar listas de marcadores, marcadores animados o basados en imágenes, capturar las coordenadas de la región que se está mirando, y mover automáticamente el mapa a una posición determinada.  


Correr con `npx react-native run-android`  


***
		
## Publicar en Git
Crear un repositorio native-google-maps  
Desproteger la rama main
Desde una terminal, ubicada en la carpeta del proyecto, ejecutar:
```
git init
git remote add origin https://gitlab.com/facundo.prieto/native-google-maps.git
git branch -M main
git add .
git commit -m "initial commit"
git push -uf origin main
```
